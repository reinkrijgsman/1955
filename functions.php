<?php

// REGISTER MENUS
function register_my_menus() {
  register_nav_menus(
    array(
      'headermenu' => __( 'Header Menu' ),
      'footermenu' => __( 'Footer Menu' )
    )
  );
}
add_action( 'init', 'register_my_menus' );



// NORMALIZE CUSTOM MENU CLASSES

add_filter('nav_menu_css_class', 'normalize_wp_classes', 10, 2);
add_filter('page_css_class', 'normalize_wp_classes', 10, 2);

function normalize_wp_classes(array $classes, $item = null){

  // old classes to be replaced with 'active'
  $replacements = array(
    'current-menu-item',
    'current-menu-parent',
    'current-menu-ancestor',
    'current_page_item',
    'current_page_parent',
    'current_page_ancestor',
  );

  // if any of the classes above are present,
  // return an array with a single class ('active')
  return array_intersect($replacements, $classes) ? array('active') : array();
}



// ADD WALKER

require_once('wp-nav-walker.php');




?>