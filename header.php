<!DOCTYPE html>
<html>
<head>
<title><?php bloginfo('name'); ?> - <?php the_title(''); ?></title>

<!-- META TAGS -->
<meta charset="UTF-8">
<meta name="description" content="<?php bloginfo('description'); ?>">
<meta name="keywords" content="">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- STYLESHEETS -->
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style.css" />

</head>
<body>

<nav class="nav">
  <div class="container">
    <div class="nav-left">
      <a class="nav-item nav-brand" href="<?php bloginfo('url'); ?>">
        <img src="<?php bloginfo('template_url'); ?>/site_img/logo.svg" alt="<?php bloginfo('name'); ?> - <?php bloginfo('description'); ?>">
      </a>
    </div>
    <!-- You need JavaScript to toggle the "is-active" class on "nav-menu" -->
    <span class="nav-toggle">
      <span></span>
      <span></span>
      <span></span>
    </span>
    <!-- Add the modifier "is-active" to display it on mobile -->
    <ul class="nav-right nav-menu">	

      <?php 
        wp_nav_menu(
          array (
            'theme_location'  => 'headermenu',
            'walker'          => new wp_nav_walker,
            'container'       => false,
            'items_wrap'      => '%3$s'
          )
        );
      ?>

    </ul>
  </div><!-- .container -->
</nav>



