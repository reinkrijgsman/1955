<?php
/*
Template Name: Template
*/
?>

<?php get_header(); ?>



<section class="hero is-large is-info">
  <div class="hero-body">
    <div class="container">
      <?php  if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <h1 class="title"><?php the_title(); ?></h1>
        <?php the_content(); ?>
        <a class="button is-large is-dark">Button</a>
      <?php endwhile; else : ?><!-- NO CONTENT --><?php endif; ?>
    </div>
  </div>
</section>


<!-- Always use as direct children of body -->
<section class="section">
  <div class="container">
    <div class="heading">
      <h1 class="title">Look at this</h1>
      <h2 class="subtitle">How is this even possible?</h2>
    </div>
  </div>
</section>

<div class="container">
  <div class="columns">
    <div class="column is-4">
      <h2>Title</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
        Nunc vitae faucibus diam, at malesuada metus. 
        Nunc posuere porttitor mauris semper tempus. 
        In blandit sodales tempor. Etiam feugiat semper dolor, 
        vitae porttitor ipsum porta sit amet. 
        Proin vestibulum est turpis, non mattis nisl scelerisque quis. 
        Morbi quis porta nibh, in vestibulum metus.</p>
    </div>
    <div class="column is-4">
      <h2>Title</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
        Nunc vitae faucibus diam, at malesuada metus. 
        Nunc posuere porttitor mauris semper tempus. 
        In blandit sodales tempor. Etiam feugiat semper dolor, 
        vitae porttitor ipsum porta sit amet. 
        Proin vestibulum est turpis, non mattis nisl scelerisque quis. 
        Morbi quis porta nibh, in vestibulum metus.</p>
    </div>
    <div class="column is-4">
      <h2>Title</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
        Nunc vitae faucibus diam, at malesuada metus. 
        Nunc posuere porttitor mauris semper tempus. 
        In blandit sodales tempor. Etiam feugiat semper dolor, 
        vitae porttitor ipsum porta sit amet. 
        Proin vestibulum est turpis, non mattis nisl scelerisque quis. 
        Morbi quis porta nibh, in vestibulum metus.</p>
    </div>
  </div><!-- .columns -->
</div><!-- .container -->



<?php get_footer(); ?>