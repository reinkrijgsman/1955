var gulp = require('gulp');
var sass = require('gulp-sass');
// var cleanCSS = require('gulp-clean-css');

gulp.task('styles', function() {
    gulp.src('sass/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css/'));
});

// //Watch task
// gulp.task('default',function() {
//     gulp.watch('sass/*.scss',['styles']);
// });

//Watch sass
gulp.task('sass', function () {
  return gulp.src('sass/base.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css/'));
});

gulp.task('sass-watch', function () {
  gulp.watch('sass/*.scss', ['sass']);
});

//Watch minify
// gulp.task('minify-css', function() {
//   return gulp.src('sass/base.scss')
//     .pipe(sass().on('error', sass.logError))
//     .pipe(cleanCSS({compatibility: 'ie8'}))
//     .pipe(gulp.dest('./css/'));
// });

// gulp.task('minify',function() {
//     gulp.watch('sass/base.scss',['minify-css']);
// });
