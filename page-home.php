<?php get_header(); ?>



<section class="hero is-fullheight is-primary header-homepage">
  <div class="hero-body">
    <div class="container">
      <?php  if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <h1 class="title"><?php the_title(); ?></h1>
        <?php the_content(); ?>
        <a class="button is-large button-base">Button</a>
      <?php endwhile; else : ?><!-- NO CONTENT --><?php endif; ?>
    </div>
  </div>
</section>




<?php get_footer(); ?>