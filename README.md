<h1>In the beginning</h1>
<p>Back in 1955, there was no git?</p>
<br />
<br />
<p>This project theme includes:</p>
<ul>
  <li>A WordPress theme</li>
  <li>Bulma CSS framework</li>
  <li>jQuery</li>
  <li>Gulp (and Gulp-SASS)</li>
</ul>
<br />
<br />
<h2>More soon...</h2>